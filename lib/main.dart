import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/login_check.dart';
import 'package:full_going_admin_app/page/menu/kickBoard/page_kick_board.dart';
import 'package:full_going_admin_app/page/page_home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FullGoingAdminApp',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],

      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: colorBackGround,
      ),
      home: LoginCheck(),         // 로그인 체크 - 완료
      //home: PageHome(),         //
     // home: PageKickBoard(pageTitleText: "킥보드 상세/변경", isUpdateMode: true),
      //home: PageKickBoard(pageTitleText: "킥보드 등록", isUpdateMode: false),

    );
  }
}