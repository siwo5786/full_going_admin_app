import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_formatter.dart';
import 'package:full_going_admin_app/config/config_form_validator.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_member/member_request.dart';
import 'package:full_going_admin_app/repository/repo_kick_board.dart';
import 'package:full_going_admin_app/repository/repo_member.dart';
import 'package:full_going_admin_app/styles/style_form_decoration_full_going.dart';
import 'package:intl/intl.dart';

class PageMember extends StatefulWidget {
  const PageMember({super.key});

  @override
  State<PageMember> createState() => _PageMemberState();
}

/*
  "memberGroup": "ROLE_ADMIN",
  "name": "string",
  "password": "string",
  "passwordRe": "string",
  "username": "string"
  "phoneNumber": "string",
  "licenceNumber": "string",

* */

class _PageMemberState extends State<PageMember> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _isPasswordVisible = true;
  bool _isRePasswordVisible = true;


  Future<void> _setMember(MemberRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().setMember(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '회원 등록 완료',
        subTitle: '회원 등록이 완료되었습니다.',
      ).call();
      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '회원 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "회원 등록"),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                FormBuilderDropdown<String>(
                  name: 'memberGroup',
                  initialValue: 'ROLE_USER',
                  decoration: InputDecoration(
                    labelText: '회원 그룹',
                    filled: true,
                    isDense: false,
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {});
                  },
                  items: const [
                    DropdownMenuItem(value: "ROLE_ADMIN", child: Text('최고관리자')),
                    DropdownMenuItem(value: "ROLE_USER", child: Text('사용자')),
                    DropdownMenuItem(value: "ROLE_OFFICE", child: Text('사무')),
                    DropdownMenuItem(value: "ROLE_SALES", child: Text('영업')),
                    DropdownMenuItem(value: "ROLE_PICKUP", child: Text('수거')),
                  ],
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  keyboardType: TextInputType.text,
                  name: 'username',
                  inputFormatters: [maskNoInputKrFormatter],
                  maxLength: 20,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration("아이디"),
                  validator: validatorOfLoginUsername,
                ), // username
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'password',
                  inputFormatters: [maskNoInputKrFormatter],
                  maxLength: 20,
                  obscureText: _isPasswordVisible,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecorationOfPassword(
                    "비밀번호",
                    GestureDetector(
                      child: _isPasswordVisible
                          ? Icon(Icons.visibility_off,
                              size: sizeSuffixIconOfPassword,
                              color: colorPrimary)
                          : Icon(Icons.visibility,
                              size: sizeSuffixIconOfPassword,
                              color: colorPrimary),
                      onTap: () {
                        setState(() {
                          _isPasswordVisible = !_isPasswordVisible;
                        });
                      },
                    ),
                  ),
                  validator: validatorOfLoginPassword,
                ), //password
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'passwordRe',
                  inputFormatters: [maskNoInputKrFormatter],
                  maxLength: 20,
                  obscureText: _isRePasswordVisible,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecorationOfPassword(
                    "비밀번호 확인",
                    GestureDetector(
                      child: _isRePasswordVisible
                          ? Icon(Icons.visibility_off,
                              size: sizeSuffixIconOfPassword,
                              color: colorPrimary)
                          : Icon(Icons.visibility,
                              size: sizeSuffixIconOfPassword,
                              color: colorPrimary),
                      onTap: () {
                        setState(() {
                          _isRePasswordVisible = !_isRePasswordVisible;
                        });
                      },
                    ),
                  ),
                  validator: (value) =>
                      _formKey.currentState?.fields['password']?.value != value
                          ? formErrorEqualPassword
                          : null,
                ), //rePassword
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'name',
                  maxLength: 20,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration("이름"),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(2,
                        errorText: formErrorMinLength(2)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ), //memberName
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'phoneNumber',
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    maskPhoneNumberFormatter
                    //13자리만 입력받도록 하이픈 2개+숫자 11개
                  ],
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration(
                    "휴대전화",
                    useHintText: true,
                    hintText: "XXX-XXXX-XXXX",
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(13,
                        errorText: formErrorMinLength(13)),
                    FormBuilderValidators.maxLength(13,
                        errorText: formErrorMaxLength(13)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'licenceNumber',
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    maskLicenseFormatter,
                    //13자리만 입력받도록 하이픈 3개+숫자 12개
                  ],
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration("운전면허번호",
                          useHintText: true, hintText: "XX-XX-XXXXXX-XX"),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.maxLength(15,
                        errorText: formErrorMaxLength(15)),
                  ]),
                ),
                ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '등록',
                      callback: () {
                        if (_formKey.currentState!.saveAndValidate()) {
                          MemberRequest request = MemberRequest(
                            _formKey.currentState!.fields['memberGroup']!.value,
                            _formKey.currentState!.fields['username']!.value,
                            _formKey.currentState!.fields['password']!.value,
                            _formKey.currentState!.fields['passwordRe']!.value,
                            _formKey.currentState!.fields['name']!.value,
                            _formKey.currentState!.fields['phoneNumber']!.value,
                            _formKey
                                .currentState!.fields['licenceNumber']!.value ?? ""
                          );

                          _setMember(request);

                        }
                      }),
                ),
                const ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '취소',
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                      callback: () {
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
