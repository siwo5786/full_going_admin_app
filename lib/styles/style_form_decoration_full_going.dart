import 'package:flutter/material.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';

class StyleFormDecorationOfFullGoing {
  InputDecoration getInputDecoration(String inputName,
      {bool useSuffixText = false,
        String suffixText = '',
        bool useHintText = false,
        String hintText = '',
        bool useCounter = false
      }) {
    return InputDecoration(
      labelText: inputName,
      suffixText: useSuffixText ? suffixText : null,
      hintText: useHintText ? hintText : null,
      filled: true,
      border: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      isDense: false,
      counter: useCounter ? null : const SizedBox(height: 0.0),
      counterText: useCounter ? "" : null,
    );
  }


  InputDecoration getInputDecorationOfPassword(String inputName, Widget? suffixIcon) {
    return InputDecoration(
      labelText: inputName,
      filled: true,
      border: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      isDense: false,
      counter: const SizedBox(height: 0.0),
      suffixIcon: suffixIcon
    );

  }


}