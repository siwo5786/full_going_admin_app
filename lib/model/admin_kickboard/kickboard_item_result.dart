import 'package:full_going_admin_app/model/admin_kickboard/kickboard_item.dart';

class KickBoardItemResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<KickBoardItem>? list;

  KickBoardItemResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory KickBoardItemResult.fromJson(Map<String, dynamic> json) {
    return KickBoardItemResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => KickBoardItem.fromJson(e)).toList() : [],
    );
  }
}