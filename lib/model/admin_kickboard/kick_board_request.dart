class KickBoardRequest {
  String modelName;
  String priceBasis;
  String dateBuy;
  double posX;
  double posY;

  KickBoardRequest(this.modelName, this.priceBasis, this.dateBuy, this.posX,
      this.posY);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['modelName'] = modelName;
    data['priceBasis'] = priceBasis;
    data['dateBuy'] = dateBuy;
    data['posX'] = posX;
    data['posY'] = posY;

    return data;
  }

/*
  "dateBuy": "string",
  "modelName": "string",
  "posX": 0,
  "posY": 0,
  "priceBasis": "BASIC"
  */

}