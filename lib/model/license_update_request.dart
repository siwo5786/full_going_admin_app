class LicenseUpdateRequest {
  String licenceNumber;

  LicenseUpdateRequest(this.licenceNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['licenceNumber'] = this.licenceNumber;

    return data;
  }
}