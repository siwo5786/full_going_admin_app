class BoardResponse{
  String wroteDateTime;
  String title;
  String name;
  String context;
  String imgName;


  BoardResponse(this.wroteDateTime, this.title, this.name, this.context, this.imgName);

  /*
    "context": "string",
    "id": 0,
    "imgName": "string",
    "name": "string",
    "title": "string",
    "wroteDateTime": "string"
  * */

  factory BoardResponse.fromJson(Map<String, dynamic> json) {
    return BoardResponse(
      json['wroteDateTime'],
      json['title'],
      json['name'],
      json['context'],
      json['imgName']
    );
  }
}