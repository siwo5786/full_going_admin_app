class BoardListItem {
  int id;
  String name;
  String title;
  String wroteDateTime;
  // String context;

  BoardListItem(this.id, this.name, this.title, this.wroteDateTime);

  factory BoardListItem.fromJson(Map<String, dynamic> json) {
    return BoardListItem(
      json['id'],
      json['name'],
      json['title'],
      json['wroteDateTime'],
      // json['context'],
    );
  }
}