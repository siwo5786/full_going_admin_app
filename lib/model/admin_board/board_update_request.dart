class BoardUpdateRequest {
  String title;
  String name;
  String context;
  String imgName;

  BoardUpdateRequest(this.title, this.name, this.context, this.imgName);

  /*
    "context": "string",
    "imgName": "string",
    "name": "string",
    "title": "string"
  * */

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['title'] = title;
    data['name'] = name;
    data['context'] = context;
    data['imgName'] = imgName;

    return data;
  }
}
