class MyHistoryItem {
  int historyId;
  String kickBoardName;
  String dateStart;
  String dateEnd;
  num resultPrice;

  MyHistoryItem(this.historyId, this.kickBoardName, this.dateStart, this.dateEnd, this.resultPrice);

  factory MyHistoryItem.fromJson(Map<String, dynamic> json) {
    return MyHistoryItem(
      json['historyId'],
      json['kickBoardName'],
      json['dateStart'],
      json['dateEnd'],
      json['resultPrice'],
    );
  }
}
