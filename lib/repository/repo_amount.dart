import 'package:dio/dio.dart';
import 'package:full_going_admin_app/config/config_api.dart';
import 'package:full_going_admin_app/functions/token_lib.dart';
import 'package:full_going_admin_app/model/common_result.dart';
import 'package:full_going_admin_app/model/remain_price_request.dart';

class RepoAmount {
  Future<CommonResult> doChargeForMember(
      int memberId, RemainPriceRequest remainPriceRequest) async {
    const String baseUrl =
        '$apiUri/remaining-amount/plus/price/member-id/{memberId}';
    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response =
        await dio.put(baseUrl.replaceAll("{memberId}", memberId.toString()),
            data: remainPriceRequest.toJson(),
            options: Options(
                followRedirects: false,
                validateStatus: (status) {
                  return status == 200;
                }));
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doChargeFullGoingPassForMember(
      {required int memberId, required String hourStr}) async {
    const String baseUrl =
        '$apiUri/remaining-amount/plus/pass/member-id/{memberId}?pass-type={hourStr}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    final response = await dio.put(
        baseUrl
            .replaceAll('{memberId}', memberId.toString())
            .replaceAll('{hourStr}', hourStr),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return CommonResult.fromJson(response.data);
  }
}
