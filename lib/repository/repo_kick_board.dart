import 'package:dio/dio.dart';
import 'package:full_going_admin_app/config/config_api.dart';
import 'package:full_going_admin_app/functions/token_lib.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kick_board_result.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kick_board_update_request.dart';
import 'package:full_going_admin_app/model/common_result.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kick_board_request.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kickboard_item_result.dart';

class RepoKickBoard {
  Future<CommonResult> setKickBoard(KickBoardRequest request) async {
    const String baseUrl = '$apiUri/kick-board/data';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<KickBoardResult> getKickBoard(int kickBoardId) async {
    final String baseUrl =
        '$apiUri/kick-board/info/kick-board-id/{kickBoardId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl.replaceAll('{kickBoardId}', kickBoardId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return KickBoardResult.fromJson(response.data);
  }



  Future<CommonResult> putKickBoard(
      int kickBoardId, String status, KickBoardUpdateRequest request) async {
    const String baseUrl =
        '$apiUri/kick-board/name-price-pos-memo-status/kick-board-id/{kickBoardId}?status={status}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl
            .replaceAll('{kickBoardId}', kickBoardId.toString())
            .replaceAll("{status}", status),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status == 200;
            }));


    return CommonResult.fromJson(response.data);
  }


  Future<KickBoardItemResult> getList({int page = 1}) async {
    final String _baseUrl = '$apiUri/kick-board/all?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response =
        await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
            options: Options(
                followRedirects: false,
                validateStatus: (status) {
                  return status == 200;
                }));
    return KickBoardItemResult.fromJson(response.data);
  }

}
